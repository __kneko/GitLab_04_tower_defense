﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour{
    [SerializeField] private int _maxHealth = 1;
    [SerializeField] private float _moveSpeed = 1f;
    [SerializeField] private SpriteRenderer _healthBar;
    [SerializeField] private SpriteRenderer _healthFill;

    private int _currentHealth;

    public Vector3 TargetPosition { get; private set; }

    public int CurrentPathIndex { get; private set; }

    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update(){
        
    }

    // called when the game object is enabled
    private void OnEnable(){
        _currentHealth = _maxHealth;
        _healthFill.size = _healthBar.size;
    }

    public void MoveToTarget(){
        transform.position = Vector3.MoveTowards(transform.position, TargetPosition, _moveSpeed * Time.deltaTime);
    }

    public void SetTargetPosition(Vector3 targetPosition){
        TargetPosition = targetPosition;
        _healthBar.transform.parent = null;

        // to change enemy rotation
        Vector3 distance = TargetPosition - transform.position;
        if (Mathf.Abs(distance.y) > Mathf.Abs(distance.x)){
            // look up
            if (distance.y > 0){
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
            }
            // look down
            else{
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, -90f));
            }
        }
        else{
            // look right (default)
            if (distance.x > 0){
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            }
            // look left
            else{
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
            }
        }

        _healthBar.transform.parent = transform;
    }

    // mark last index on path
    public void SetCurrentPathIndex(int currentIndex){
        CurrentPathIndex = currentIndex;
    }

    public void ReduceEnemyHealth(int damage){
        _currentHealth -= damage;
        float hpPercent = (float) _currentHealth / _maxHealth;
        
        AudioPlayer.Instance.PlaySFX("hit-enemy");

        if (_currentHealth <= 0){
            gameObject.SetActive(false);
            AudioPlayer.Instance.PlaySFX("enemy-die");
        }

        // health bar handler
        _healthFill.size = new Vector2(_healthBar.size.x * hpPercent, _healthBar.size.y);
    }
}
